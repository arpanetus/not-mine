<!DOCTYPE>
<?php
session_start();
include("functions/functions.php");
?>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>My online shop</title>
    <link rel="stylesheet" href="/styles/style.css"/>
    <link rel="stylesheet" href="/css/bootstrap.css"/>
    <link rel="stylesheet" href="/css/custom.css">
    <!--    <link rel="stylesheet" href="css/bootstrap-grid.css"/>-->
    <!--    <link rel="stylesheet" href="css/bootstrap-reload.css" />-->
</head>

<body>


<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <!--    <a class="navbar-brand" href="#">Navbar</a>-->
    <div class="container">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="../index.php">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../all_products.php">All products </a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="../customer/my_account.php">My account <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../customer_register.php">Sign up</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../cart.php">Shopping Cart </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Contact us</a>
                </li>
            </ul>
            <form method="get" class="form-inline my-2 my-lg-0" action="results.php" enctype="multipart/form-data">
                <input class="form-control mr-sm-2" type="search" aria-label="Search" name="user_query"
                       placeholder="Search a product">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit" name="search" value="Search">Search
                </button>
            </form>
        </div>
    </div>
</nav>
<div class="container">
    <div class="alert alert-dark" role="alert">

        <?php
        if (isset($_SESSION['customer_email'])) {
            echo "<b>Welcome:</b>" . $_SESSION['customer_email'] . "<b style='color:#483D8B;'>Your</b>";
        } else {
            echo "<b>Welcome Guest:</b>";

        }
        ?>

        <b style="color:#483D8B">Shopping Cart</b> - Total Items: <?php total_items(); ?>
        Total Price:<?php total_price(); ?> <b><a href="cart.php" style="color:#483D8B">Go to Cart</a></b>

        <?php
        if (!isset($_SESSION['customer_email'])) {

            echo "<a href='checkout.php' style='color:orange;'>Login</a>";
        } else {
            echo "<a href='logout.php' style='color:orange;'>Logout</a>";

        }

        ?>
    </div>

    <!--            --><?php
    //            $user = $_SESSION['customer_email'];
    //
    //            $get_img = "select * from customers where customer_email='$user'";
    //
    //            $run_img = mysqli_query($con, $get_img);
    //
    //            $row_img = mysqli_fetch_array($run_img);
    //
    //            $c_image = $row_img['customer_image'];
    //
    //            $c_name = $row_img['customer_name'];
    //
    //            echo "<p style='text-align: center;'<img src='customer_images/$c_image' width='150' height='150'/>";
    //
    //            ?>

    <div class="row">
        <div class="col-md-3">
            <ul class="list-group">
                <li class="list-group-item"><a href="my_account.php?my_orders">My orders</a></li>
                <li class="list-group-item"><a href="my_account.php?edit_account">Edit Account</a></li>
                <li class="list-group-item"><a href="my_account.php?change_pass">Change Password</a></li>
                <li class="list-group-item"><a href="my_account.php?delete_account">Delete Account</a></li>
                <li class="list-group-item"><a href="logout.php">Logout</a></li>

            </ul>
        </div>

        <div class="col-md-9">
            <div id="shopping_cart">
				<span style="float:right; font-size:16px; padding:5px; line-height:35px;">

				<?php
                if (isset($_SESSION['customer_email'])) {
                    echo "<b>Welcome:</b>" . $_SESSION['customer_email'];

                }
                ?>

                <?php
                if (!isset($_SESSION['customer_email'])) {

                    echo "<a href='checkout.php' style='color:orange;'>Login</a>";
                } else {
                    echo "<a href='logout.php' style='color:orange;'>Logout</a>";

                }


                ?>

				</span>
            </div>

            <div id="products_box">

                <?php
                if (!isset($_GET['my_orders'])) {
                    if (!isset($_GET['edit_account'])) {
                        if (!isset($_GET['change_pass'])) {
                            if (!isset($_GET['delete_account'])) {
                                echo "
			<h2 style='padding:20px;'>Welcome: $c_name </h2>
			<b>You can see your orders progress by clicking this <a href='my_account.php?my_orders'>link</a></b>";
                            }
                        }
                    }
                }
                ?>

                <?php
                if (isset($_GET['edit_account'])) {
                    include("edit_account.php");
                }

                if (isset($_GET['change_pass'])) {
                    include("change_pass.php");
                }

                if (isset($_GET['delete_account'])) {
                    include("delete_account.php");
                }
                ?>

            </div>

        </div>
    </div>
</div>


</body>
</html>