<?php
session_start();

if (!isset($_SESSION['user_email'])) {

    echo "<script>window.open('login.php?not_admin=You are not an Admin!','_self')</script>";
} else { ?>

    <!DOCTYPE>

    <html>
    <head>
        <title>This is Admin Panel</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!--        <link rel="stylesheet" href="styles/style.css" media="all"/>-->
        <link rel="stylesheet" href="/css/bootstrap.css"/>
        <link rel="stylesheet" href="/css/custom.css">

    </head>

    <body>

    <nav class="navbar navbar-expand-lg navbar-light bg-light align-items-center">
        <div class="container align-items-center" style="text-align:center!important;">
            <h1 style="text-align:center!important; width: 100%">Willkommen im Admin-Panel</h1>
        </div>
    </nav>
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <!--                <div class="card">-->
                <div class="card-title" style="top: 40%;">
                    <h2 style="color:#d92148; text-align:center; top: 40%;"><?php echo @$_GET['logged_in']; ?></h2>
                    <?php
                    if (isset($_GET['insert_product'])) {
                        include("insert_product.php");
                    }

                    if (isset($_GET['view_products'])) {
                        include("view_products.php");
                    }

                    if (isset($_GET['edit_pro'])) {
                        include("edit_pro.php");
                    }

                    if (isset($_GET['insert_cat'])) {
                        include("insert_cat.php");
                    }

                    if (isset($_GET['view_cats'])) {
                        include("view_cats.php");
                    }

                    if (isset($_GET['edit_cat'])) {
                        include("edit_cat.php");
                    }

                    if (isset($_GET['insert_brand'])) {
                        include("insert_brand.php");
                    }

                    if (isset($_GET['view_brands'])) {
                        include("view_brands.php");
                    }

                    if (isset($_GET['edit_brand'])) {
                        include("edit_brand.php");
                    }

                    if (isset($_GET['view_customers'])) {
                        include("view_customers.php");
                    }
                    ?></div>
                <!--                </div>-->
            </div>
            <div class="col-md-3">
                <ul class="list-group">
                <li class="list-group-item active" style="text-align:center;">Manage Content</li>
                <li class="list-group-item"> <a href="index.php?insert_product">Insert New Product</a></li>
                <li class="list-group-item"> <a href="index.php?view_products">View All Products</a></li>
                <li class="list-group-item"> <a href="index.php?insert_cat">Insert New Category</a></li>
                <li class="list-group-item"> <a href="index.php?view_cats">View All Categories</a></li>
                <li class="list-group-item"> <a href="index.php?insert_brand">Insert New Collections</a></li>
                <li class="list-group-item"> <a href="index.php?view_brands">View All Collections</a></li>
                <li class="list-group-item"> <a href="index.php?view_customers">View Customers</a></li>
                <li class="list-group-item"> <a href="index.php?view_orders">View Orders</a></li>
                <li class="list-group-item"> <a href="index.php?view_payments">View Payments</a></li>
                <li class="list-group-item"> <a href="logout.php">Admin Logout</a></li>
                </ul>
            </div>
        </div>

    </div>

    </body>
    </html>

<?php } ?>