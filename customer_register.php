<!DOCTYPE>
<?php
session_start();
include("functions/functions.php");
include("includes/db.php");
?>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>My online shop</title>
<!--    <link rel="stylesheet" href="styles/style.css" media="all"/>-->

    <link rel="stylesheet" href="css/bootstrap.css"/>
    <link rel="stylesheet" href="css/custom.css">
    <!--    <link rel="stylesheet" href="css/bootstrap-grid.css"/>-->
    <!--    <link rel="stylesheet" href="css/bootstrap-reload.css" />-->

</head>

<body>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <!--    <a class="navbar-brand" href="#">Navbar</a>-->
    <div class="container">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="all_products.php">All products </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="customer/my_account.php">My account </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="customer_register.php">Sign up</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="cart.php">Shopping Cart </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Contact us</a>
                </li>
            </ul>
            <form method="get" class="form-inline my-2 my-lg-0" action="results.php" enctype="multipart/form-data">
                <input class="form-control mr-sm-2" type="search" aria-label="Search" name="user_query"
                       placeholder="Search a product">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit" name="search" value="Search">Search
                </button>
            </form>
        </div>
    </div>

</nav>

<!--Main Wrapper starts here-->
<div class="container">
    <div class="alert alert-dark" role="alert">

        <?php
        if (isset($_SESSION['customer_email'])) {
            echo "<b>Welcome:</b>" . $_SESSION['customer_email'] . "<b style='color:#483D8B;'>Your</b>";
        } else {
            echo "<b>Welcome Guest:</b>";

        }
        ?>

        <b style="color:#483D8B">Shopping Cart</b> - Total Items: <?php total_items(); ?>
        Total Price:<?php total_price(); ?> <b><a href="cart.php" style="color:#483D8B">Go to Cart</a></b>

        <?php
        if (!isset($_SESSION['customer_email'])) {
            echo "<a href='checkout.php' style='color:orange;'>Login</a>";
        } else {
            echo "<a href='logout.php' style='color:orange;'>Logout</a>";

        }

        ?>
    </div>
    <div class="row">
        <div class="col-md-3">
            <ul class="list-group">
                <li class="list-group-item active" disabled>Categories</li>
                <?php getCats(); ?>
            </ul>
            <ul class="list-group">
                <li class="list-group-item active" disabled>Collections</li>
                <?php getBrands(); ?>
            </ul>
        </div>

        <div class="col-md-9">

            <form action="customer_register.php" method="post" enctype="multipart/form-data">

                <table class="table" align="center" width="750">

                    <tr align="center">
                        <td colspan="6"><h2>Create an Account</h2></td>
                    </tr>

                    <tr>
                        <td align="right">Customer Name:</td>
                        <td><input class="form-control" type="text" name="c_name" required/></td>
                    </tr>

                    <tr>
                        <td align="right">Customer Email:</td>
                        <td><input class="form-control" type="text" name="c_email" required/></td>
                    </tr>

                    <tr>
                        <td align="right">Customer Password:</td>
                        <td><input class="form-control" type="password" name="c_pass" required/></td>
                    </tr>

                    <tr>
                        <td align="right">Customer Image:</td>
                        <td><input class="custom-file" type="file" name="c_image" required/></td>
                    </tr>

                    <tr>
                        <td align="right">Customer Country:</td>
                        <td>
                            <select class="custom-select" name="c_country">
                                <option>Select a Country</option>
                                <option>Kazakhstan</option>
                                <option>Russia</option>
                                <option>USA</option>
                                <option>China</option>
                                <option>Japan</option>
                                <option>United Kingdom</option>
                            </select>
                        </td>
                    </tr>

                    <tr>
                        <td align="right">Customer City:</td>
                        <td><input class="form-control" type="text" name="c_city" required/></td>
                    </tr>

                    <tr>
                        <td align="right">Customer Contact:</td>
                        <td><input class="form-control" type="text" name="c_contact" required/></td>
                    </tr>

                    <tr>
                        <td align="right">Customer Address:</td>
                        <td><input class="form-control" type="text" name="c_address" required/></td>
                    </tr>

                    <tr align="center">
                        <td colspan="6"><input class="btn btn-info" type="submit" name="register" value="Create Account"/></td>
                    </tr>

                </table>
            </form>

        </div>
    </div>
</div>
</body>
</html>
<?php
if (isset($_POST['register'])) {

    $ip = getIp();

    $c_name = $_POST['c_name'];
    $c_email = $_POST['c_email'];
    $c_pass = $_POST['c_pass'];
    $c_image = $_FILES['c_image']['name'];
    $c_image_tmp = $_FILES['c_image']['tmp_name'];
    $c_country = $_POST['c_country'];
    $c_city = $_POST['c_city'];
    $c_contact = $_POST['c_contact'];
    $c_address = $_POST['c_address'];

    move_uploaded_file($c_image_tmp, "customer/customer_images/$c_image");

    $insert_c = "insert into customers (customer_ip, customer_name, customer_email, customer_pass, customer_country, customer_city, customer_contact, customer_address, customer_image) values ('$ip', '$c_name', '$c_email', '$c_pass', '$c_country', '$c_city', '$c_contact', 'c_address', 'c_image')";

    $run_c = mysqli_query($con, $insert_c);

    $sel_cart = "select * from cart where ip_add='$ip'";

    $run_cart = mysqli_query($con, $sel_cart);

    $check_cart - mysqli_num_rows($run_cart);

    if ($check_cart == 0) {

        $_SESSION['customer_email'] = $c_email;

        echo "<script>alert('Account has been created successfully, Thanks!')</script>";
        echo "<script>window.open('customer/my_account.php', '_self')</script>";

    } else {
        $_SESSION['customer_email'] = $c_email;

        echo "<script>alert('Account has been created successfully, Thanks!')</script>";
        echo "<script>window.open('checkout.php', '_self')</script>";


    }
}


?>