<!DOCTYPE>
<?php
session_start();
include("functions/functions.php");
?>
<html>
<head>
    <title>My online shop</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!--    <link rel="stylesheet" href="styles/style.css" media="all"/>-->
    <title>My online shop</title>
    <link rel="stylesheet" href="css/bootstrap.css"/>
    <link rel="stylesheet" href="css/custom.css">

</head>

<body>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <!--    <a class="navbar-brand" href="#">Navbar</a>-->
    <div class="container">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="index.php">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="all_products.php">All products </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="customer/my_account.php">My account </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="customer_register.php">Sign up</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="cart.php">Shopping Cart <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Contact us</a>
                </li>
            </ul>
            <form method="get" class="form-inline my-2 my-lg-0" action="results.php" enctype="multipart/form-data">
                <input class="form-control mr-sm-2" type="search" aria-label="Search" name="user_query"
                       placeholder="Search a product">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit" name="search" value="Search">Search
                </button>
            </form>
        </div>
    </div>
</nav>


<div class="container">
    <div class="alert alert-dark" role="alert">

        <?php
        if (isset($_SESSION['customer_email'])) {
            echo "<b>Welcome:</b>" . $_SESSION['customer_email'] . "<b style='color:#483D8B;'>Your</b>";
        } else {
            echo "<b>Welcome Guest:</b>";

        }
        ?>

        <b style="color:#483D8B">Shopping Cart</b> - Total Items: <?php total_items(); ?>
        Total Price:<?php total_price(); ?> <b><a href="cart.php" style="color:#483D8B">Go to Cart</a></b>

        <?php
        if (!isset($_SESSION['customer_email'])) {

            echo "<a href='checkout.php' style='color:orange;'>Login</a>";
        } else {
            echo "<a href='logout.php' style='color:orange;'>Logout</a>";

        }

        ?>
    </div>

    <div class="row">
        <div class="col-md-3">
            <ul class="list-group">
                <li class="list-group-item active" disabled>Categories</li>
                <?php getCats(); ?>
            </ul>
            <ul class="list-group">
                <li class="list-group-item active" disabled>Collections</li>
                <?php getBrands(); ?>
            </ul>
        </div>

        <div class="col-md-9">
            <div id="products_box">

                <form action="" method="post" enctype="multipart/form-data">
                    <!--Table: displays products in the cart-->
                    <table class="table" align="center" width="700" bgcolor="#deecef">

                        <tr align="center">
                            <th scope="col">Remove</th>
                            <th scope="col">Product (S)</th>
                            <th scope="col">Quantity</th>
                            <th scope="col">Total Price</th>
                        </tr>

                        <?php
                        $total = 0;

                        global $con;

                        $ip = getIp();//getting IP address by function getIP

                        $sel_price = "select * from cart where ip_add='$ip'";

                        $run_price = mysqli_query($con, $sel_price);

                        while ($p_price = mysqli_fetch_array($run_price)) {

                            $pro_id = $p_price['p_id'];

                            $pro_price = "select * from products where product_id='$pro_id'";

                            $run_pro_price = mysqli_query($con, $pro_price);

                            while ($pp_price = mysqli_fetch_array($run_pro_price)) {

                                $product_price = array($pp_price['product_price']);

                                $product_title = $pp_price['product_title'];

                                $product_image = $pp_price['product_image'];

                                $single_price = $pp_price['product_price'];

                                $values = array_sum($product_price);

                                $total += $values;

                                ?>
                                <tr align="center">
                                    <td><input type="checkbox" name="remove[]" value="<?php echo $pro_id; ?>"/></td>
                                    <td><?php echo $product_title; ?><br>
                                        <img src="admin_area/product_images/<?php echo $product_image; ?>" width="60"
                                             height="60"/></td>
                                    <?php
                                    if (isset($_POST['update_cart'])) {

                                        $qty = $_POST['qty'];

                                        $update_qty = "update cart set qty = '$qty'";
                                        $run_qty = mysqli_query($con, $update_qty);

                                        $_SESSION['qty'] = $qty;

                                        $total = $total * $qty;
                                    }

                                    ?>
                                    <td><input type="text" size="6" value="<?php echo $_SESSION['qty']; ?>"/>
                                    </td>

                                    <td><?php echo "$" . $single_price; ?></td>
                                </tr>


                            <?php }
                        } ?>

                        <tr align="right">
                            <td colspan="4"><b>Sub Total:</b></td>
                            <td><?php echo "$" . $total; ?></td>
                        </tr>

                        <tr align="center">
                            <td colspan="1"><input type="submit" class="btn" name="update_cart" value="Update Cart"/></td>
                            <td><input class="btn" type="submit" name="continue" value="Continue Shopping"/></td>
                            <td>
                                <button class="btn"><a href="checkout.php" style="text-decoration:none; color:black;">Checkout</a>
                                </button>
                            </td>
                        </tr>


                    </table>
                </form>

                <?php

                function updatecart()
                {

                    global $con;

                    $ip = getIp();

                    if (isset($_POST['update_cart'])) {

                        foreach ($_POST['remove'] as $remove_id) {

                            $delete_product = "delete from cart where p_id='$remove_id' AND ip_add='$ip'";

                            $run_delete = mysqli_query($con, $delete_product);

                            if ($run_delete) {

                                echo "<script>window.open('cart.php','_self')</script>";

                            }

                        }

                    }
                    if (isset($_POST['continue'])) {

                        echo "<script>window.open('index.php','_self')</script>";
                    }

                    echo @$up_cart = updatecart();

                }

                ?>
            </div>
        </div>

    </div>
</div>

</body>
</html>