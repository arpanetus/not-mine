<?php

$con = mysqli_connect("localhost", "root", "", "ecommerce");

if (mysqli_connect_errno()) {
    echo "The connection was not established: " . mysqli_connect_error();
}
//getting the user ip address
function getIp()
{
    $ip = $_SERVER['REMOTE_ADDR'];

    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    }

    return $ip;
}

//creating the cart
function cart()
{

    if (isset($_GET['add_cart'])) {

        global $con;

        $ip = getIp();

        $pro_id = $_GET['add_cart'];

        $check_pro = "select * from cart where ip_add='$ip' AND p_id='$pro_id'";

        $run_check = mysqli_query($con, $check_pro);

        if (mysqli_num_rows($run_check) > 0) {
            echo "";
        } else {

            $insert_pro = "insert into cart (p_id, ip_add) values ('$pro_id','$ip')";

            $run_pro = mysqli_query($con, $insert_pro);

            echo "<script>window.open('index.php','_self')</script>";
        }
    }
}

//getting the total added items
function total_items()
{

    if (isset($_GET['add_cart'])) {

        global $con;

        $ip = getIp();

        $get_items = "select * from cart where ip_add='$ip'";

        $run_items = mysqli_query($con, $get_items);

        $count_items = mysqli_num_rows($run_items);

    } else {
        global $con;

        $ip = getIp();

        $get_items = "select * from cart where ip_add='$ip'";

        $run_items = mysqli_query($con, $get_items);

        $count_items = mysqli_num_rows($run_items);
    }
    echo $count_items;
}

//getting the total price of the items in shopping cart

function total_price()
{

    $total = 0;

    global $con;

    $ip = getIp();

    $sel_price = "select * from cart where ip_add='$ip'";

    $run_price = mysqli_query($con, $sel_price);

    while ($p_price = mysqli_fetch_array($run_price)) {

        $pro_id = $p_price['p_id'];

        $pro_price = "select * from products where product_id='$pro_id'";

        $run_pro_price = mysqli_query($con, $pro_price);

        while ($pp_price = mysqli_fetch_array($run_pro_price)) {

            $product_price = array($pp_price['product_price']);

            $values = array_sum($product_price);

            $total += $values;


        }


    }

    echo " $" . $total;
}

//getting the categories
function getCats()
{

    global $con;

    $get_cats = "select * from categories";

    $run_cats = mysqli_query($con, $get_cats);

    while ($row_cats = mysqli_fetch_array($run_cats)) {

        $cat_id = $row_cats['cat_id'];
        $cat_title = $row_cats['cat_title'];

        echo "<li class='list-group-item'><a href ='index.php?cat=$cat_id'>$cat_title</a></li>";
    }
    //WHILE loop ends here
}

//getting the brands
function getBrands()
{

    global $con;

    $get_brands = "select * from brands";

    $run_brands = mysqli_query($con, $get_brands);

    while ($row_brands = mysqli_fetch_array($run_brands)) {

        $brand_id = $row_brands['brand_id'];
        $brand_title = $row_brands['brand_title'];

        echo "<li class='list-group-item'><a href ='index.php?brand=$brand_id'>$brand_title</a></li>";
    }
    //WHILE loop ends here
}

function getPro()
{

    If (!isset($_GET['cat'])) {
        if (!isset($_GET['brand'])) {

            global $con;

            $get_pro = "select * from products order by RAND() LIMIT 0,6";//getting 6 of the latest products for home page

            $run_pro = mysqli_query($con, $get_pro);

            while ($row_pro = mysqli_fetch_array($run_pro)) {

                $pro_id = $row_pro['product_id'];
                $pro_cat = $row_pro['product_cat'];
                $pro_brand = $row_pro['product_brand'];
                $pro_title = $row_pro['product_title'];
                $pro_price = $row_pro['product_price'];
                $pro_image = $row_pro['product_image'];

                echo "
					<div class=\"card\" style=\"width: 13rem;\">
                      <img class=\"card-img-top rounded mx-auto card-img\" src='admin_area/product_images/$pro_image'>
                      <div class=\"card-body\">
                        <h5 class=\"card-title\">$pro_title</h5>
                        <!-- <p class=\"card-text\">Some quick example text to build on the card title and make up the bulk of the card's content.</p> -->
                        <div class='d-flex flex-row'>
                        <a href=\"details.php?pro_id=$pro_id\" class=\"card-link\" >Details</a>
                        <a href='index.php?add_cart=$pro_id' class=\"btn btn-danger card-link\" >Add to Cart</a>
                        </div>
                      </div>
                    </div>	
                ";
            }
        }
    }
}


//displaying cats
function getCatPro()
{

    If (isset($_GET['cat'])) {

        $cat_id = $_GET['cat'];

        global $con;

        $get_cat_pro = "select * from products where product_cat='$cat_id'";//getting 6 of the latest products for home page

        $run_cat_pro = mysqli_query($con, $get_cat_pro);

        $count_cats = mysqli_num_rows($run_cat_pro);

        If ($count_cats == 0) {
            echo "
                  <div class=\"card alert alert-danger\" role=\"alert\">
                    No products were found in this<a href=\"#\" class=\"alert-link\"> category</a>.
                  </div>
                  
                  ";
        }

        while ($row_cat_pro = mysqli_fetch_array($run_cat_pro)) {

            $pro_id = $row_cat_pro['product_id'];
            $pro_cat = $row_cat_pro['product_cat'];
            $pro_brand = $row_cat_pro['product_brand'];
            $pro_title = $row_cat_pro['product_title'];
            $pro_price = $row_cat_pro['product_price'];
            $pro_image = $row_cat_pro['product_image'];


            echo "<div class=\"card\" style=\"width: 13rem;\">
                      <img class=\"card-img-top rounded mx-auto card-img\" src='admin_area/product_images/$pro_image'>
                      <div class=\"card-body\">
                        <h5 class=\"card-title\">$pro_title</h5>
                        <!-- <p class=\"card-text\">Some quick example text to build on the card title and make up the bulk of the card's content.</p> -->
                        <div class='d-flex flex-row'>
                        <a href=\"details.php?pro_id=$pro_id\" class=\"card-link\" >Details</a>
                        <a href='index.php?add_cart=$pro_id' class=\"btn btn-danger card-link\" >Add to Cart</a>
                        </div>
                      </div>
                    </div>";
        }

    }
}


//displaying brands
function getBrandPro()
{

    If (isset($_GET['brand'])) {

        $brand_id = $_GET['brand'];

        global $con;

        $get_brand_pro = "select * from products where product_brand='$brand_id'";//getting 6 of the latest products for home page

        $run_brand_pro = mysqli_query($con, $get_brand_pro);

        $count_brands = mysqli_num_rows($run_brand_pro);

        If ($count_brands == 0) {
            echo "
                  <div class=\"car alert alert-danger\" role=\"alert\">
                    No products associated with this <a href=\"#\" class=\"alert-link\">brand </a>were found.
                  </div>
                  ";
        }

        while ($row_brand_pro = mysqli_fetch_array($run_brand_pro)) {

            $pro_id = $row_brand_pro['product_id'];
            $pro_cat = $row_brand_pro['product_cat'];
            $pro_brand = $row_brand_pro['product_brand'];
            $pro_title = $row_brand_pro['product_title'];
            $pro_price = $row_brand_pro['product_price'];
            $pro_image = $row_brand_pro['product_image'];


            echo "<div class=\"card\" style=\"width: 13rem;\">
                      <img class=\"card-img-top rounded mx-auto card-img\" src='admin_area/product_images/$pro_image'>
                      <div class=\"card-body\">
                        <h5 class=\"card-title\">$pro_title</h5>
                        <!-- <p class=\"card-text\">Some quick example text to build on the card title and make up the bulk of the card's content.</p> -->
                        <div class='d-flex flex-row'>
                        <a href=\"details.php?pro_id=$pro_id\" class=\"card-link\" >Details</a>
                        <a href='index.php?add_cart=$pro_id' class=\"btn btn-danger card-link\" >Add to Cart</a>
                        </div>
                      </div>
                    </div>	";
        }

    }
}

?>