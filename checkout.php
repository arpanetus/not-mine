<!DOCTYPE>
<?php
session_start();
include("functions/functions.php");
?>
<html>
<head>
    <title>My online shop</title>

    <link rel="stylesheet" href="styles/style.css" media="all"/>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>My online shop</title>
    <link rel="stylesheet" href="css/bootstrap.css"/>
    <link rel="stylesheet" href="css/custom.css">

</head>

<body>


<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <!--    <a class="navbar-brand" href="#">Navbar</a>-->
    <div class="container">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="all_products.php">All products </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="customer/my_account.php">My account </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="customer_register.php">Sign up</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="cart.php">Shopping Cart </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Contact us</a>
                </li>
            </ul>
            <form method="get" class="form-inline my-2 my-lg-0" action="results.php" enctype="multipart/form-data">
                <input class="form-control mr-sm-2" type="search" aria-label="Search" name="user_query"
                       placeholder="Search a product">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit" name="search" value="Search">Search
                </button>
            </form>
        </div>
    </div>

</nav>

<div class="container">

    <div class="alert alert-dark" role="alert">

        <?php
        if (isset($_SESSION['customer_email'])) {
            echo "<b>Welcome:</b>" . $_SESSION['customer_email'] . "<b style='color:#483D8B;'>Your</b>";
        } else {
            echo "<b>Welcome Guest:</b>";

        }
        ?>

        <b style="color:#483D8B">Shopping Cart</b> - Total Items: <?php total_items(); ?>
        Total Price:<?php total_price(); ?> <b><a href="cart.php" style="color:#483D8B">Go to Cart</a></b>

        <?php
        if (!isset($_SESSION['customer_email'])) {

            echo "<a href='checkout.php' style='color:orange;'>Login</a>";
        } else {
            echo "<a href='logout.php' style='color:orange;'>Logout</a>";

        }

        ?>
    </div>

    <div class="row">

        <div class="col-md-3">
            <ul class="list-group">
                <li class="list-group-item active" disabled>Categories</li>
                <?php getCats(); ?>
            </ul>
            <ul class="list-group">
                <li class="list-group-item active" disabled>Collections</li>
                <?php getBrands(); ?>
            </ul>
        </div>
        <div class="col-md-9">
            <div id="products_box">

                <?php
                if (!isset($_SESSION['customer_email'])) {

                    include("customer_login.php");
                } else {

                    include("payment.php");

                }


                ?>
            </div>

        </div>
    </div>
</div>


</body>
</html>